import React from 'react';
import Card from './card';
import Numberformat from 'react-number-format';
const Covid19 = (props) => {
    const { totalRecover,
        totalDeath,
        totalconform,
        country} = props
    
    return (
        <div>
            <div>
                <h1 style={{textTransform:"capitalize"}}>{country === '' ? 'world wide corona report' : country}</h1>

                <div style={{
                    display: 'flex',
                    justifyContent: 'center',
                    color:'green'
                }}>
                    <Card>
                        <span>Total Death</span><br />
                        <span>{<Numberformat 
                        value={totalDeath} 
                        displayType={"text"} color={"green"}
                        thousandSeparator={true}
                        />}</span>
                    </Card>
                    <Card>
                        <span>Total Recovered</span><br />
                        <span>{<Numberformat 
                        value={totalRecover} 
                        displayType={"text"}
                        thousandSeparator={true}
                        />}</span>
                    </Card>
                    <Card>
                        <span>Total Conform</span><br />
                        <span>{<Numberformat 
                        value={totalconform} 
                        displayType={"text"}
                        thousandSeparator={true}
                        />}</span>
                    </Card>
                </div>
            </div>
        </div>
    )
}

export default Covid19;
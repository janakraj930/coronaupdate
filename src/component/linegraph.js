import React from 'react';
import {Line} from 'react-chartjs-2';
const Linegraph=(props)=>{
    return(
       <div style={{
           width:'900px',
           height:'700px',
           margin:'70px auto ',
           color:"red"
           

       }}>
           <Line data={
                {
                    labels: props.label.map(l=>l.substr(0,10)),
                    datasets: [
                      {
                        label: 'Corona Speed' ,
                        data:props.yAxis,
                        fill: true,
                        lineTesion:5,
                        backgroundColor: 'yellow',
                        borderColor: 'rgba(255, 99, 132, 0.10)',
                        borderCapStyle:'butt',
                        borderDash:[],
                        borderDashoffset:0.1,
                        borderJoinStyle:'miter',
                        pointBorderColor:'red',
                        pointBackgroundColor:'green',
                        pointBorderWidth:2,
                        pointHoverRadius:10,
                        pointHoverBackgroundColor:'green',
                        pointHoverBorderColor:'red',
                        pointHoverBorderWidth:'red',
                        pointRadius:3,
                        pointHitRadius:15


                      },
                    ],
                  }
           } />
       </div>

    )
}

export default Linegraph;
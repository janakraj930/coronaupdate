import axios from 'axios';
const Axios = axios.create({
    baseURL: `https://api.covid19api.com/`
})

export default Axios;
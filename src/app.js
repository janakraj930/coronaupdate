import React, { useEffect, useState } from 'react';
import './app.css';
import Covid19 from './component/covid19';
import Linegraph from './component/linegraph';
import axios from './component/axios';
function Welcome() {
    const [totalconform, settotalconform] = useState(0);
    const [totalDeath, settotalDeath] = useState(0);
    const [totalRecover, settotalRecover] = useState(0);
    const [loading, setloading] = useState(false)
    const [covid19, setcovid19] = useState({})
    const [days, setdays] = useState(7)
    const [country, setcountry] = useState('')
    const [coronaCountAr, setcoronaCountAr] = useState([])
    const [label, setlabel] = useState([])



    useEffect(() => {
        setloading(true)
        axios.get(`/summary`)
            .then(res => {
                setloading(false)
                console.log(res)
                if (res.status === 200) {
                    settotalconform(res.data.Global.TotalConfirmed);
                    settotalDeath(res.data.Global.TotalDeaths);
                    settotalRecover(res.data.Global.TotalRecovered);
                    setcovid19(res.data)


                }
            })
            .catch(err => {
                console.log(err);
            })

    }, []);
    const formateDate = (date) => {
        const d = new Date(date)
        const year = d.getFullYear();
        const month = `0${d.getMonth() + 1}`.slice(-2)
        const _date = d.getDate()
        return `${year}-${month}-${_date}`
    }
    const countryHandler = (e) => {

        setcountry(e.target.value)
        const d = new Date()
        const to = formateDate(d)
        const from = formateDate(d.setDate(d.getDate() - days))
        // console.log(from,to)
        getcoronareport(e.target.value, from, to)
    }
    const daysHandler = (e) => {
        const d = new Date()
        const to = formateDate(d)
        const from = formateDate(d.setDate(d.getDate() - e.target.value))
        setdays(e.target.value)
        getcoronareport(country,from,to)
    }
    const getcoronareport = (countySlug, from, to) => {
        axios.get(`/country/${countySlug}/status/confirmed?from=${from}T00:00:00Z&to=${to}T00:00:00Z`)
            .then(res => {
                console.log(res)

                const yaxiscoronaCount = res.data.map(d => d.Cases)
                const xAxislabel=res.data.map(d=>d.Date)
                const covid19Detail = covid19.Countries.find(country => country.Slug === countySlug)
                settotalconform(covid19Detail.TotalConfirmed)
                settotalRecover(covid19Detail.TotalRecovered)
                settotalDeath(covid19Detail.TotalDeaths)
                setcoronaCountAr(yaxiscoronaCount)
                setlabel(xAxislabel);
            })
            .catch(err => {
                console.log(err)
            })
    }
    if (loading) {
        return <p>Featch data from server</p>
    }
    return (
        <>
            <div className="App">
                <Covid19
                    totalRecover={totalRecover}
                    totalDeath={totalDeath}
                    totalconform={totalconform}
                    country={country}
                />
                <br />
                <div>
                    <select value={country} onChange={countryHandler}>
                        <option value=""> Choose your country</option>
                        {
                            covid19.Countries && covid19.Countries.map(country =>
                                <option key={country.Slug} value={country.Slug}>{country.Country}</option>
                            )}
                    </select>
                    <select value={days} onChange={daysHandler}>
                        <option value="7">last 7 days</option>
                        <option value="14">Last 14 Days</option>
                        <option value="20">Last 20 Days</option>
                        <option value="30">last 30 days</option>
                        <option value="2">last 2 days</option>
                    </select>



                </div>
                <Linegraph
                    yAxis={coronaCountAr} 
                    label={label}/>
            </div>
        </>
    )
}

export const App = Welcome;